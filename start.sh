#!/usr/bin/env bash
export SERVICE_HOST=http://localhost:8082
gunicorn -w 4 -b 0.0.0.0:8082 app:app